public class AccountWithInterest extends BasicAccount {

	public boolean withdraw(int amount) {
		if (getBalance() - amount >= 0 && super.withdraw(amount)) {
			return true;
		}
		return false;
	}

	public void yearEnd() {
		int balance = getBalance();
		double interest = (balance * 1.05) - balance; 
		super.deposit((int) interest);
	}
}
