public class FeeBasedAccount extends BasicAccount {
	private int transactions = 0;

	public void deposit(int amount) {
		super.deposit(amount);
		transactions++;
	}

	public boolean withdraw(int amount) {
		if (super.withdraw(amount)) {
			transactions++;
			return true;
		}
		return false;
	}

	public void yearEnd() {
		super.withdraw(transactions);
		transactions = 0;
	}
}