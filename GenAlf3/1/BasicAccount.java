public abstract class BasicAccount implements Account {
	private int balance = 0;

	public void deposit(int amount) {
		if (amount > 0) {
			balance += amount;
		}
		else throw new IllegalArgumentException("Argument must not be negative");
	}

	public boolean withdraw(int amount) {
		if (amount > 0) {
			balance -= amount;
			return true;
		}
		else throw new IllegalArgumentException("Argument must not be negative");
	}

	public int getBalance() {
		return balance;
	}

	public abstract void yearEnd();
}