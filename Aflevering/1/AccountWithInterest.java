public class AccountWithInterest extends BasicAccount {

	public boolean withdraw(int amount) {
		if (!negativeInt(amount) && getBalance() - amount >= 0) {
			setBalance(getBalance() - amount);
			return true;
		}
		return false;
	}

	public void yearEnd() {
		double newBalance = getBalance() * 1.05;
		setBalance((int) newBalance);
	}
}
