public class FeeBasedAccount extends BasicAccount {
	private int transactions = 0;

	public void deposit(int amount) {
		if (!negativeInt(amount)) {
			setBalance(getBalance() + amount);
			transactions++;
		}
	}

	public boolean withdraw(int amount) {
		if (!negativeInt(amount)) {
			setBalance(getBalance() - amount);
			transactions++;
			return true;
		}
		return false;
	}

	public void yearEnd() {
		setBalance(getBalance() - transactions);
		transactions = 0;
	}
}