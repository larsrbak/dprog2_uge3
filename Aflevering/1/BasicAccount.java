public abstract class BasicAccount implements Account {
	private int balance = 0;

	public void deposit(int amount) {
		if (!negativeInt(amount))
			balance += amount;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public boolean negativeInt(int i) {
		if (i < 0) {
			throw new IllegalArgumentException("Argument must not be negative");
		}
		return false;
	}

	public abstract void yearEnd();

	public abstract boolean withdraw(int amount);

}