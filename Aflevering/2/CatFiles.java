import java.io.*;
import java.util.Scanner;

public class CatFiles {
	public static void main(String[] args) {
		new CatFiles(args);
	}

	/**
	 * 			Takes the content of all files (if found) except the last. The last file
	 * 			has the content of the other files. If the output file don't exists, one
	 * 			will be created. If one does exist it will be overwritten.
	 * 
	 * @param	Stringarray of file names
	 */
	public CatFiles(String[] files) {
		checkArguments(files);
		String output = findOutputFile(files);
		String[] inputs = findInputFiles(files);
		String content = readFile(inputs);
		writeFile(content, output);
	}

	/**
	 * @param 	Content to be printed, Name of output file where content is printed.
	 * @return 	Returns true if file where successfully created, otherwise false
	 */
	public boolean writeFile(String content, String output) {
		try {
			FileWriter writer = new FileWriter(output);
			writer.write(content);
			writer.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @param 	Names of input files to be read
	 * @return 	Content of all input files
	 */
	public String readFile(String[] inputs) {
		String fileContent = "";
		for (String s : inputs) {
			try {
				Scanner reader = new Scanner(new FileReader(s));
				while (reader.hasNextLine()) {
					String line = reader.nextLine();
					fileContent += line + " \r ";
				}
				fileContent += " \n "; // Creates a new line at end of file.
				reader.close();
			} catch (FileNotFoundException e) {
				System.out.println("Exeption: " + s + " was not found, and therefore ignored.");
			}
		}
		return fileContent;
	}

	/**
	 * @param 	Stringarray of file names
	 * @return 	Returns all file names except the last
	 */
	public String[] findInputFiles(String[] files) {
		String[] inputFiles = new String[files.length - 1];
		for (int i = 0; i < files.length - 1; i++) {
			inputFiles[i] = files[i];
		}
		return inputFiles;
	}

	/**
	 * @param 	Stringarray of file names
	 * @return 	Returns the name of the last file
	 */
	public String findOutputFile(String[] files) {
		return files[files.length - 1]; // returns the last element of the array
	}

	/**
	 * 			Throws an exception if argument in main method is invalid
	 * 
	 * @param	Stringarray of file names
	 */
	public void checkArguments(String[] files) {
		if (files.length < 2) {
			throw new IllegalArgumentException("Please enter atleast one inputfile and put the output file as the last parameter");
		}
	}

}
